package com.example.demo.persistence;

import javax.persistence.*;
import java.time.LocalDate;
//https://thoughts-on-java.org/hibernate-tips-map-multiple-entities-same-table/
@Entity(name = "Experience")
@Table(name = "EXPIRIANCE")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(discriminatorType = DiscriminatorType.CHAR,
name = "EXP_ACADEMIC")
@DiscriminatorValue("U")
@SequenceGenerator(name = "SEQ_EXPIRIANCE", initialValue = 1, allocationSize = 10)
public abstract class Experience {

    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_EXPIRIANCE")
    @Id
    @Column(name = "EXP_ID",
    updatable = false,
    nullable = false)
    private Long id;

    @Column(name = "EXP_FROM")
    private LocalDate from;

    @Column(name = "EXP_TO")
    private LocalDate to;

    @Column(name = "EXP_NAME")
    private String name;

    @Column(name = "EXP_DESCRIPTION")
    private String description;

    @ManyToOne
    @JoinColumn(name = "EXP_PERSONAL_ID")
    private PersonalData personalData;

    public Long getId() {
        return id;
    }

    public LocalDate getFrom() {
        return from;
    }

    public void setFrom(LocalDate from) {
        this.from = from;
    }

    public LocalDate getTo() {
        return to;
    }

    public void setTo(LocalDate to) {
        this.to = to;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public PersonalData getPersonalData() {
        return personalData;
    }

    public void setPersonalData(PersonalData personalData) {
        this.personalData = personalData;
    }
}
