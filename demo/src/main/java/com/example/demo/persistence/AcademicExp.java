package com.example.demo.persistence;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity(name = "AcademicExp")
//@Table(name = "EXPIRIANCE")
@DiscriminatorValue("Y")
public class AcademicExp extends Experience{

    @Column(name = "EXP_FIELD_OF_STUDY")
    private String fieldOfStudy;

    public String getFieldOfStudy() {
        return fieldOfStudy;
    }

    public void setFieldOfStudy(String fieldOfStudy) {
        this.fieldOfStudy = fieldOfStudy;
    }
}
