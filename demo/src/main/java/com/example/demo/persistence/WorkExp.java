package com.example.demo.persistence;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity(name = "WorkExp")
@DiscriminatorValue("N")
public class WorkExp extends Experience {

    @Column(name = "EXP_POSITION")
    private String position;

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }
}
