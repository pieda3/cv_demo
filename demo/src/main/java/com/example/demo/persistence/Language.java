package com.example.demo.persistence;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity(name = "LANGUAGE")
@DiscriminatorValue("L")
public class Language extends Abilitie{

    @Column(name = "AB_LLVL")
    private String level;

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }
}
