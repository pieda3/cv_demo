package com.example.demo.persistence;

import javax.persistence.*;

@Entity(name = "Abilitie")
@Table(name = "ABILITIE")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "AB_TYPE",
        discriminatorType = DiscriminatorType.CHAR)
@DiscriminatorValue("A")
@SequenceGenerator(name = "SEQ_ABILITIE", initialValue = 1, allocationSize = 10)
public class Abilitie {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_ABILITIE")
    @Column(name = "AB_ID")
    private double id;

    @Column(name = "AB_NAME")
    private String name;

    @Column(name = "AB_DETAILS")
    private String aditionalInfo;

    @ManyToOne
    @JoinColumn(name = "AB_PERSONAL_ID")
    private PersonalData personalData;

    public double getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAditionalInfo() {
        return aditionalInfo;
    }

    public void setAditionalInfo(String aditionalInfo) {
        this.aditionalInfo = aditionalInfo;
    }

    public PersonalData getPersonalData() {
        return personalData;
    }

    public void setPersonalData(PersonalData personalData) {
        this.personalData = personalData;
    }
}
