package com.example.demo.persistence;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "PERSONAL_DATA")
@SequenceGenerator(name="SEQ_PERSONAL_DATA", initialValue=1, allocationSize=10)
public class PersonalData {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_PERSONAL_DATA")
    @Column(name = "ID")
    private Long id;

    @Column(name = "FIRST_NAME")
    private String firstName;

    @Column(name = "MIDDLE_NAME")
    private String middleName;

    @Column(name = "LAST_NAME")
    private String lastName;

    @Column(name = "EMAIL")
    private String email;

    @Column(name = "PHONE")
    private String phoneNumber;

    @Column(name = "URL")
    private String site;

    @Column(name = "INTERESTS")
    private String interests;

    @OneToMany(mappedBy = "personalData")
    private List<Experience> expirience;

    @OneToMany(mappedBy = "personalData")
    private List<Abilitie> abilities;

    public Long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getInterests() {
        return interests;
    }

    public void setInterests(String interests) {
        this.interests = interests;
    }

    public List<Experience> getExpirience() {
        return expirience;
    }

    public void setExpirience(List<Experience> expirience) {
        this.expirience = expirience;
    }

    public List<Abilitie> getAbilities() {
        return abilities;
    }

    public void setAbilities(List<Abilitie> abilities) {
        this.abilities = abilities;
    }
}
