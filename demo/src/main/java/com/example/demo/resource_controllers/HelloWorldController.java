package com.example.demo.resource_controllers;

import com.example.demo.data_representation.Greeting;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.concurrent.atomic.AtomicLong;
//https://spring.io/guides/gs/actuator-service/
@Controller
public class HelloWorldController {
    private static final String templat = "Witaj %s!";
    private final AtomicLong counter = new AtomicLong();

    @GetMapping("/hello")
    @ResponseBody
    public Greeting sayHello(
            @RequestParam(name = "name", required = false, defaultValue = "przyjacielu")
             String name){
        return new Greeting(counter.incrementAndGet(), String.format(templat, name));
    }

}
